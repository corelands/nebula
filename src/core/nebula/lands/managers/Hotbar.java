package core.nebula.lands.managers;

import core.nebula.lands.api.ItemCreator;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum Hotbar {

    LEAVE(2, new ItemCreator(Material.RED_BED).setName("§cLeave")),
    CHOOSE(4, new ItemCreator(Material.WHITE_WOOL).setName("§bChoose Team")),
    VOTE(6, new ItemCreator(Material.SUNFLOWER).setName("§eMap Vote"));

    private ItemCreator itemCreator;
    private int slot;

    Hotbar(int slot, ItemCreator itemCreator) {
        this.itemCreator = itemCreator;
        this.slot = slot;
    }

    public ItemStack getItem() {
        return this.itemCreator.getItem();
    }

    public int getSlot() {
        return this.slot;
    }

}
