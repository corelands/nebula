package core.nebula.lands.managers.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class NebulaScoreboardManager {

    private Scoreboard board;
    private Objective obj;
    private Player p;

    public NebulaScoreboardManager(Player p) {
        this.board = Bukkit.getScoreboardManager().getNewScoreboard();
        this.obj = board.registerNewObjective("Nebula", "CoreLands");
        this.obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.obj.setDisplayName("§5Nebula");
        this.p = p;
    }

    public void setScoreboard() {

        obj.getScore("§a ").setScore(10);

        obj.getScore("§5Purple Team").setScore(9);

        Team p_right_core = board.registerNewTeam("p_right_core");
        p_right_core.addEntry("§a§b");
        p_right_core.setSuffix("§a✓ §fRight Core");
        obj.getScore("§a§b").setScore(8);

        Team p_left_core = board.registerNewTeam("p_left_core");
        p_left_core.addEntry("§a§c");
        p_left_core.setSuffix("§a✓ §fLeft Core");
        obj.getScore("§a§c").setScore(7);

        obj.getScore("§b ").setScore(6);

        obj.getScore("§6Orange Team").setScore(5);

        Team o_right_core = board.registerNewTeam("o_right_core");
        o_right_core.addEntry("§b§b");
        o_right_core.setSuffix("§a✓ §fRight Core");
        obj.getScore("§b§b").setScore(4);

        Team o_left_core = board.registerNewTeam("o_left_core");
        o_left_core.addEntry("§b§c");
        o_left_core.setSuffix("§a✓ §fLeft Core");
        obj.getScore("§b§c").setScore(3);

        obj.getScore("§c ").setScore(2);

        Team souls = board.registerNewTeam("souls");
        souls.addEntry("§eSouls: ");
        souls.setSuffix("§f0");
        obj.getScore("§eSouls: ").setScore(1);

        Team time = board.registerNewTeam("time");
        time.addEntry("§eTime: ");
        time.setSuffix("§f00:00");
        obj.getScore("§eTime: ").setScore(0);

        Team purple = board.registerNewTeam("purple");
        Team orange = board.registerNewTeam("orange");

        purple.setPrefix("§5");
        orange.setPrefix("§6");

        this.p.setScoreboard(board);


    }
}
