package core.nebula.lands.managers.scoreboard;

import core.nebula.lands.Core;
import core.nebula.lands.managers.cores.Cores;
import core.nebula.lands.managers.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardAnimator extends BukkitRunnable {

    int time = 0;

    @Override
    public void run() {
        time++;
        for (Player all : Bukkit.getOnlinePlayers()) {
            Scoreboard board = all.getScoreboard();

            if(Core.getGame().getPlayerData(all).getTeam().equals(Team.ORANGE)) {
                board.getTeam("orange").addEntry(all.getName());
            } else {
                board.getTeam("purple").addEntry(all.getName());
            }

            String p_right_core = Cores.PURPLE_RIGHT.isActive() ? "§a✓ §fRight Core" : "§c✗ §fRight Core";
            board.getTeam("p_right_core").setSuffix(p_right_core);

            String p_left_core = Cores.PURPLE_LEFT.isActive() ? "§a✓ §fLeft Core" : "§c✗ §fLeft Core";
            board.getTeam("p_left_core").setSuffix(p_left_core);

            String o_right_core = Cores.ORANGE_RIGHT.isActive() ? "§a✓ §fRight Core" : "§c✗ §fRight Core";
            board.getTeam("o_right_core").setSuffix(o_right_core);

            String o_left_core = Cores.ORANGE_LEFT.isActive() ? "§a✓ §fLeft Core" : "§c✗ §fLeft Core";
            board.getTeam("o_left_core").setSuffix(o_left_core);

            board.getTeam("time").setSuffix("§f" + getTime());
        }
    }

    public String getTime() {
        int minutes = 0;
        int seconds = 0;

        minutes = time/60;
        seconds = time%60;

        String timeString = "";

        if(minutes < 10) {
            timeString+="0";
        }
        timeString+=minutes;

        timeString+=":";

        if(seconds < 10) {
            timeString+="0";
        }
        timeString+=seconds;

        return timeString;
    }

}
