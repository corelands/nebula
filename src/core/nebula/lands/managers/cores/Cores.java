package core.nebula.lands.managers.cores;

import core.nebula.lands.Core;
import core.nebula.lands.api.InstantFirework;
import core.nebula.lands.game.GameState;
import core.nebula.lands.managers.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public enum Cores {

    ORANGE_RIGHT(new CoresManagers("orange_right").getLocation(), Team.ORANGE),
    ORANGE_LEFT(new CoresManagers("orange_left").getLocation(), Team.ORANGE),
    PURPLE_RIGHT(new CoresManagers("purple_right").getLocation(), Team.PURPLE),
    PURPLE_LEFT(new CoresManagers("purple_left").getLocation(), Team.PURPLE);

    private Location location;
    private boolean active;
    private Team team;

    Cores(Location location, Team team) {
        this.location = location;
        this.team = team;
    }

    public boolean baseContains(Block block) {
        return (this.location.getY()-1 == block.getY()) && (this.location.distance(block.getLocation()) < 2);
    }

    public boolean isUpVertical(Block block) {
        return this.location.getX() == block.getX() && this.location.getZ() == block.getZ() && this.location.getY() < block.getY();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
        if(!active)
            this.destroy();
    }

    public Team getTeam() {
        return this.team;
    }

    public boolean equalsTo(Block block) {
        return this.location.equals(block.getLocation());
    }

    public Location getLocation() {
        return this.location;
    }

    private void destroy() {
        int orangeLifes = 0;
        int purpleLifes = 0;
        for(Cores core : Cores.values()) {
            if(core.isActive()) {
                if (core.getTeam().equals(Team.ORANGE)) {
                    orangeLifes+=1;
                } else {
                    purpleLifes+=1;
                }
            }
        }

        if(orangeLifes == 0) {

            Core.getGame().setGameState(GameState.RESTARTING);

            for(Player all : Bukkit.getOnlinePlayers()) {
                all.sendTitle("§5§lPURPLE WINS", "", 20, 40, 20);
            }
            end(Color.PURPLE);

        } else if(purpleLifes == 0) {

            for(Player all : Bukkit.getOnlinePlayers()) {
                all.sendTitle("§6§lORANGE WINS", "", 20, 40, 20);
            }
            end(Color.ORANGE);

        }

    }

    private void end(Color color) {

        Core.getGame().setGameState(GameState.RESTARTING);

        for(Player all : Bukkit.getOnlinePlayers()) {
            all.setAllowFlight(true);
            all.setFlying(true);
        }

        new BukkitRunnable() {
            int time = 0;
            @Override
            public void run() {
                time++;
                for(Player all : Bukkit.getOnlinePlayers()) {
                    new InstantFirework(FireworkEffect.builder().flicker(false).trail(true).with(FireworkEffect.Type.BALL).withColor(color).withFade(Color.WHITE).build(), all.getLocation());
                }
                if(time == 5) {

                    ByteArrayOutputStream b = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(b);
                    try {
                        out.writeUTF("Connect");
                        out.writeUTF("lobby");
                    } catch (IOException eee) {
                    }
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        all.sendPluginMessage(Core.getInstance(), "BungeeCord", b.toByteArray());
                    }
                }
                if(time == 7) {
                    Bukkit.getServer().shutdown();
                }
            }
        }.runTaskTimer(Core.getInstance(), 0, 20);
    }

}
