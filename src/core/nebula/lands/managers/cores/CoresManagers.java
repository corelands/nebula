package core.nebula.lands.managers.cores;

import core.nebula.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

class CoresManagers {

    private String core;

    CoresManagers(String core) {
        this.core = core;
    }

    Location getLocation() {

        String map = Core.getGame().getGameConfig().getActiveMap();
        FileConfiguration config = Core.getInstance().getConfig();

        World world = Bukkit.getWorld(config.getString(map + ".cores." + core + ".world"));
        double x = config.getDouble(map + ".cores." + core + ".x");
        double y = config.getDouble(map + ".cores." + core + ".y");
        double z = config.getDouble(map + ".cores." + core + ".z");
        return new Location(world, x, y, z);
    }

}
