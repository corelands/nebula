package core.nebula.lands.managers.teams;

import core.nebula.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class TeamManager {

    private String team;

    public TeamManager(String team) {
        this.team = team;
    }

    public Location getSpawn() {
        String map = Core.getGame().getGameConfig().getActiveMap();

        FileConfiguration fileConfiguration = Core.getInstance().getConfig();

        return new Location(Bukkit.getWorld(fileConfiguration.getString(map + ".spawnLocation." + team + ".world")),
                fileConfiguration.getDouble(map + ".spawnLocation." + team + ".x"),
                fileConfiguration.getDouble(map + ".spawnLocation." + team + ".y"),
                fileConfiguration.getDouble(map + ".spawnLocation." + team + ".z"),
                fileConfiguration.getLong(map + ".spawnLocation." + team + ".yaw"),
                fileConfiguration.getLong(map + ".spawnLocation." + team + ".pitch"));
    }

}
