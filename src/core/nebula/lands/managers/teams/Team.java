package core.nebula.lands.managers.teams;

import core.nebula.lands.Core;
import core.nebula.lands.data.PlayerData;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public enum Team {

    ORANGE(Color.ORANGE, ChatColor.GOLD),
    PURPLE(Color.PURPLE, ChatColor.DARK_PURPLE);

    private Color color;
    private ChatColor chatColor;
    static HashMap<Team, ArrayList<Player>> teams = new HashMap<>();

    Team(Color color, ChatColor chatColor) {
        this.color = color;
        this.chatColor = chatColor;
    }

    public void addPlayer(Player player) {
        this.debug();

        PlayerData playerData = Core.getGame().getPlayerData(player);
        if(playerData.getTeam() != null) {
            playerData.getTeam().removePlayer(player);
        }
        ArrayList<Player> players = teams.get(this);
        players.add(player);
        teams.put(this, players);
    }

    public void removePlayer(Player player) {
        this.debug();
        teams.get(this).remove(player);
    }

    public ArrayList<Player> getPlayers() {
        this.debug();
        return teams.get(this);
    }

    public Color getColor() {
        this.debug();
        return this.color;
    }

    public boolean containsPlayer(Player player) {
        this.debug();
        if(teams.get(this) == null)
            return false;
        return teams.get(this).contains(player);
    }

    private void debug() {
        teams.computeIfAbsent(this, emptyList -> new ArrayList<>());
    }

    public Location getSpawnLocation() {
        this.debug();
        if(this == ORANGE) {
            return new TeamManager("orange").getSpawn();
        }
        return new TeamManager("purple").getSpawn();
    }

    public List<String> getPlayersList() {
        this.debug();
        List<String> list = new ArrayList<>();
        for(Player player : teams.get(this)) {
            list.add(this.chatColor + player.getName());
        }
        return list;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

}
