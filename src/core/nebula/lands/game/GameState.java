package core.nebula.lands.game;

public enum GameState {

    WAITING,
    STARTING,
    IN_GAME,
    RESTARTING;

}
