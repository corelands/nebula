package core.nebula.lands.game;

import core.nebula.lands.Core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameConfig {

    private String prefix = "§7[§dNebula§7]", activeMap;
    private List<String> maps;
    private Map<String, Integer> voting;

    public GameConfig() {
        maps = new ArrayList<>();
        voting = new HashMap<>();
        activeMap = "Dragons";
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getActiveMap() {
        return this.activeMap;
    }

    public int getStartTime() {
        return 61;
    }

    public int getMinPlayers() {
        return 2;
    }

    public void addMap(String map) {
        this.maps.add(map);
        this.voting.put(map, 0);
    }

    public int getMapVotes(String map) {
        return this.voting.get(map);
    }

    public void addMapVote(String map) {
        this.voting.put(map, this.voting.get(map)+1);
    }

    public void removeMapVote(String map) {
        this.voting.put(map, this.voting.get(map)-1);
    }

    public List<String> getMaps() {
        return this.maps;
    }

    public String getRoomID() {
        return Core.getInstance().getConfig().getString("room");
    }

    public void setActiveMap(String map) {
        this.activeMap = map;
    }

}
