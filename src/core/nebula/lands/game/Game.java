package core.nebula.lands.game;

import core.nebula.lands.api.MySQL;
import core.nebula.lands.data.PlayerData;
import core.nebula.lands.managers.cores.Cores;
import core.nebula.lands.managers.teams.Team;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Game {

    private GameConfig gameConfig;
    private HashMap<Player, PlayerData> cache;
    private MySQL mySQL;

    public Game() {
        this.gameConfig = new GameConfig();
        this.cache = new HashMap<>();
        this.mySQL = new MySQL("localhost", "corelands", "debian-sys-maint", "WnCqLjDzdtGlr1CL", 3306);
        this.mySQL.connect();
    }

    private GameState gameState;

    public GameState getGameState() {
        return this.gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public GameConfig getGameConfig() {
        return this.gameConfig;
    }

    public PlayerData addPlayerData(Player player) {
        this.cache.put(player, new PlayerData(player));
        return this.cache.get(player);
    }

    public PlayerData getPlayerData(Player player) {
        return this.cache.get(player);
    }

    public void removePlayerData(Player player) {
        if(getPlayerData(player).getMapVote() != null) {
            getGameConfig().removeMapVote(getPlayerData(player).getMapVote());
        }
        this.cache.remove(player);
        Team.ORANGE.removePlayer(player);
        Team.PURPLE.removePlayer(player);
        if(gameState.equals(GameState.IN_GAME)) {
            if (Team.ORANGE.getPlayers().size() == 0) {
                Cores.ORANGE_LEFT.setActive(false);
                Cores.ORANGE_RIGHT.setActive(false);
            } else if (Team.PURPLE.getPlayers().size() == 0) {
                Cores.PURPLE_LEFT.setActive(false);
                Cores.PURPLE_RIGHT.setActive(false);
            }
        }
    }

    public MySQL getMySQL() {
        return this.mySQL;
    }

}
