package core.nebula.lands.runnables;

import core.nebula.lands.Core;
import core.nebula.lands.data.PlayerData;
import core.nebula.lands.game.GameState;
import core.nebula.lands.managers.cores.Cores;
import core.nebula.lands.managers.scoreboard.ScoreboardAnimator;
import core.nebula.lands.managers.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;

public class StartTimer extends BukkitRunnable {

    private int time, startTime;

    public StartTimer() {
        time = Core.getGame().getGameConfig().getStartTime();
        startTime = time-1;
    }

    public void run() {
        time--;

        for(Player all : Bukkit.getOnlinePlayers()) {
            all.setLevel(time);
            all.setExp((float) ((time*1.0)/(startTime*1.0)));

            if((time == 60 || time == 30 || time == 15 || time == 10 || time <= 5) && time > 0) {
                all.playSound(all.getLocation(), Sound.UI_BUTTON_CLICK, 1f, 1f);
            }
        }
        if((time == 60 || time == 30 || time == 15 || time == 10 || time <= 5) && time > 0) {
            Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Starting in §e" + time + " §7seconds.");
        }

        if(time == 0) {
            if(Bukkit.getOnlinePlayers().size() >= Core.getGame().getGameConfig().getMinPlayers()) {
                start();
            } else {
                stop();
            }
            this.cancel();
        }

    }

    private void start() {
        String winnerMap = Core.getGame().getGameConfig().getMaps().get(0);

        int maxVotes = 0;

        for(String map : Core.getGame().getGameConfig().getMaps()) {
            if(Core.getGame().getGameConfig().getMapVotes(map) > maxVotes) {
                maxVotes = Core.getGame().getGameConfig().getMapVotes(map);
                winnerMap = map;
            }
        }

        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Map §e" + winnerMap);
        Core.getGame().getGameConfig().setActiveMap(winnerMap);

        Team.ORANGE.getSpawnLocation();
        Team.PURPLE.getSpawnLocation();

        for(Cores core : Cores.values()) {
            core.setActive(true);
        }

        Bukkit.getServer().setWhitelist(true);

        this.autoBalance();

        for(Player player : Bukkit.getOnlinePlayers()) {
            PlayerData playerData = Core.getGame().getPlayerData(player);
            for(Player all : Bukkit.getOnlinePlayers()) {
                if(playerData.getTeam().equals(Team.ORANGE)) {
                    player.getScoreboard().getTeam("orange").addPlayer(all);
                } else {
                    player.getScoreboard().getTeam("purple").addPlayer(all);
                }
            }
            playerData.spawn();
        }

        Core.getGame().setGameState(GameState.IN_GAME);
        Core.getGame().getMySQL().setRoomState(GameState.IN_GAME.toString());
        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Try to destroy the enemy team cores");
        new ScoreboardAnimator().runTaskTimer(Core.getInstance(), 0, 20);
    }

    private void autoBalance() {

        int maxPlayersTeam = (Bukkit.getOnlinePlayers().size()/2) + (Bukkit.getOnlinePlayers().size() % 2);

        if(Team.ORANGE.getPlayers().size() > maxPlayersTeam) {
            for(int i = 0; i < maxPlayersTeam-Team.ORANGE.getPlayers().size(); i++) {
                Team.PURPLE.addPlayer(Team.ORANGE.getPlayers().get(i));
                Team.ORANGE.getPlayers().get(0).sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Team Auto-balance changed you to team §5Purple§7.");
            }
        }

        if(Team.PURPLE.getPlayers().size() > maxPlayersTeam) {
            for(int i = 0; i < maxPlayersTeam-Team.PURPLE.getPlayers().size(); i++) {
                Team.PURPLE.getPlayers().get(0).sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Team Auto-balance changed you to team §6Orange§7.");
                Team.ORANGE.addPlayer(Team.ORANGE.getPlayers().get(i));
            }
        }

        int countNeedOrange = (Bukkit.getOnlinePlayers().size()/2) - Team.ORANGE.getPlayers().size(), count = 0;

        for(Player all : Bukkit.getOnlinePlayers()) {
            all.setExp(0);
            all.setLevel(0);
            PlayerData playerData = Core.getGame().getPlayerData(all);

            if(playerData.getTeam() == null) {
                if(count < countNeedOrange) {
                    Team.ORANGE.addPlayer(all);
                    all.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Team Auto-balance changed you to team §6Orange§7.");
                } else {
                    all.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Team Auto-balance changed you to team §5Purple§7.");
                    Team.PURPLE.addPlayer(all);
                }

                count++;
            }
        }
    }

    private void stop() {
        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Not enough players to start the game");
        Core.getGame().setGameState(GameState.WAITING);
        Bukkit.getServer().setWhitelist(false);
    }

}
