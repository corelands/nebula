package core.nebula.lands.commands;

import core.nebula.lands.Core;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class CmdSetCore implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.isOp()) {
            Player player = (Player) commandSender;
            Block block = player.getWorld().getBlockAt(player.getLocation());

            String core = args[0].toLowerCase();

            String map = args[1];
            FileConfiguration config = Core.getInstance().getConfig();

            config.set(map + ".cores." + core + ".world", block.getWorld().getName());
            config.set(map + ".cores." + core + ".x", block.getX());
            config.set(map + ".cores." + core + ".y", block.getY());
            config.set(map + ".cores." + core + ".z", block.getZ());

            block.setType(Material.BEACON);

            Core.getInstance().saveConfig();
            Core.getInstance().reloadConfig();

            player.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §aYou have set the core §d" + args[0] + "§a for the map §e" + map);

        }
        return false;
    }
}
