package core.nebula.lands.listeners;

import core.nebula.lands.Core;
import core.nebula.lands.data.PlayerData;
import core.nebula.lands.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ListenerPlayerChat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        e.setCancelled(true);

        if (Core.getGame().getGameState().equals(GameState.IN_GAME) || Core.getGame().getGameState().equals(GameState.RESTARTING)) {
            PlayerData playerData = Core.getGame().getPlayerData(e.getPlayer());
            Bukkit.broadcastMessage(playerData.getTeam().getChatColor() + e.getPlayer().getName() + "§7: " + e.getMessage());
        } else {
            Bukkit.broadcastMessage("§e" + e.getPlayer().getName() + "§7: " + e.getMessage());
        }
    }

}
