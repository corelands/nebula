package core.nebula.lands.listeners;

import core.nebula.lands.Core;
import core.nebula.lands.data.PlayerData;
import core.nebula.lands.game.GameState;
import core.nebula.lands.managers.cores.Cores;
import core.nebula.lands.managers.teams.Team;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class ListenerMainProtection implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
        if(Core.getGame().getGameState().equals(GameState.IN_GAME)) {
            for(Team team : Team.values()) {
                if(distance(team.getSpawnLocation(), e.getBlock().getLocation()) < 5) {
                    e.getPlayer().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7You can't build near the spawn");
                    e.setCancelled(true);
                }
            }

            for(Cores core : Cores.values()) {
                if(core.isActive() && core.isUpVertical(e.getBlock())) {
                    e.getPlayer().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7You can't place blocks here");
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {

        PlayerData playerData = Core.getGame().getPlayerData(e.getPlayer());

        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
        if(Core.getGame().getGameState().equals(GameState.IN_GAME)) {
            for(Team team : Team.values()) {
                if(distance(team.getSpawnLocation(), e.getBlock().getLocation()) < 5) {
                    e.getPlayer().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7You can't build near the spawn");
                    e.setCancelled(true);
                }
            }

            for(Cores core : Cores.values()) {
                if(core.isActive() && core.baseContains(e.getBlock())) {
                    e.getPlayer().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7You can't break this block");
                    e.setCancelled(true);
                }
            }

            if(e.getBlock().getType().equals(Material.BEACON)) {
                e.setDropItems(false);
                for(Cores core : Cores.values()) {
                    if(core.equalsTo(e.getBlock()) && core.getTeam().equals(playerData.getTeam())) {
                        e.getPlayer().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7You can't break your own core.");
                        e.setCancelled(true);
                    } else if(core.equalsTo(e.getBlock())) {

                        if(playerData.getTeam().equals(Team.ORANGE)) {
                            for(Player player : Team.ORANGE.getPlayers()) {
                                player.sendTitle("§aENEMY CORE DESTROYED", "§7" + e.getPlayer().getName(), 20, 40, 20);
                                player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1f, 1f);
                            }
                            for(Player player : Team.PURPLE.getPlayers()) {
                                player.sendTitle("§cCORE DESTROYED", "§7" + e.getPlayer().getName(), 20, 40, 20);
                                player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1f, 1f);
                            }
                        } else {
                            for(Player player : Team.PURPLE.getPlayers()) {
                                player.sendTitle("§aENEMY CORE DESTROYED", "§7" + e.getPlayer().getName(), 20, 40, 20);
                                player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1f, 1f);
                            }
                            for(Player player : Team.ORANGE.getPlayers()) {
                                player.sendTitle("§cCORE DESTROYED", "§7" + e.getPlayer().getName(), 20, 40, 20);
                                player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1f, 1f);
                            }
                        }

                        core.setActive(false);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPickupItem(PlayerPickupItemEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onSpawnMobs(CreatureSpawnEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    public double distance(Location loc1, Location loc2) {
        return Math.abs(Math.pow(loc1.getX()-loc2.getX(), 2)+Math.pow(loc1.getZ()-loc2.getZ(), 2)+Math.pow(loc1.getZ()-loc2.getZ(), 2));
    }

}
