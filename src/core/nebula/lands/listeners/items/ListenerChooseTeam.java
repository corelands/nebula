package core.nebula.lands.listeners.items;

import core.nebula.lands.Core;
import core.nebula.lands.api.ItemCreator;
import core.nebula.lands.game.GameState;
import core.nebula.lands.managers.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class ListenerChooseTeam implements Listener {

    private String name = "§eChoose Team";

    private Inventory getInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(player, 9, this.name);

        int limit = 0;
        int players = Bukkit.getOnlinePlayers().size();

        limit = (players % 2 == 0) ? players/2 : players/2+1;

        inventory.setItem(2, new ItemCreator(Material.ORANGE_WOOL).setName("§6Orange Team §e(" + Team.ORANGE.getPlayers().size() + "/" + limit + ")").setLore(Team.ORANGE.getPlayersList()).getItem());
        inventory.setItem(6, new ItemCreator(Material.PURPLE_WOOL).setName("§5Purple Team §e(" + Team.PURPLE.getPlayers().size() + "/" + limit + ")").setLore(Team.PURPLE.getPlayersList()).getItem());

        return inventory;
    }

    @EventHandler
    public void onPlayerUseItem(PlayerInteractEvent e) {
        if(Core.getGame().getGameState().equals(GameState.WAITING) || Core.getGame().getGameState().equals(GameState.STARTING)) {
            if(e.getItem() != null && (e.getItem().getType().equals(Material.WHITE_WOOL) || e.getItem().getType().equals(Material.ORANGE_WOOL) || e.getItem().getType().equals(Material.PURPLE_WOOL))) {
                e.getPlayer().openInventory(getInventory(e.getPlayer()));
            }
        }
    }

    @EventHandler
    public void chooseTeam(InventoryClickEvent e) {
        if(e.getView().getTitle().equals(name)) {

            int limit;
            int players = Bukkit.getOnlinePlayers().size();

            limit = (players % 2 == 0) ? players/2 : players/2+1 ;

            e.setCancelled(true);
            Player player = (Player) e.getWhoClicked();
            switch (e.getSlot()) {
                case 2:
                    if(Team.ORANGE.getPlayers().size() < limit) {
                        Team.ORANGE.addPlayer(player);
                        player.openInventory(this.getInventory(player));
                        player.getInventory().setItem(4, new ItemCreator(Material.ORANGE_WOOL).setName("§bChoose Team").getItem());
                        player.updateInventory();
                    } else {
                        player.closeInventory();
                        player.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §cThis team is full");
                    }
                    break;
                case 6:
                    if(Team.PURPLE.getPlayers().size() < limit) {
                        Team.PURPLE.addPlayer(player);
                        player.openInventory(this.getInventory(player));
                        player.getInventory().setItem(4, new ItemCreator(Material.PURPLE_WOOL).setName("§bChoose Team").getItem());
                        player.updateInventory();
                    } else {
                        player.closeInventory();
                        player.sendMessage(Core.getGame().getGameConfig().getPrefix()+ " §cThis team is full");
                    }
                    break;
            }
        }
    }

}
