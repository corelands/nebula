package core.nebula.lands.listeners;

import core.nebula.lands.Core;
import core.nebula.lands.data.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class ListenerPlayerQuit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);

        PlayerData playerData = Core.getGame().getPlayerData(e.getPlayer());

        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + playerData.getName() + " §7left the game §e(" + (Bukkit.getOnlinePlayers().size()-1) + "/" + Bukkit.getMaxPlayers() + ")");
        Core.getGame().removePlayerData(e.getPlayer());
        Core.getGame().getMySQL().setRoomPlayers(Bukkit.getOnlinePlayers().size()-1);
        Core.getGame().getMySQL().removeGlobalPlayer();
    }

}
