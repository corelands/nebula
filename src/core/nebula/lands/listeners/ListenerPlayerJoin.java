package core.nebula.lands.listeners;

import core.nebula.lands.Core;
import core.nebula.lands.data.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ListenerPlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);

        e.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100D);

        PlayerData playerData = Core.getGame().addPlayerData(e.getPlayer());

        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + playerData.getName() + " §7joined the game §e(" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers() + ")");
        Core.getGame().getMySQL().setRoomPlayers(Bukkit.getOnlinePlayers().size());
        Core.getGame().getMySQL().addGlobalPlayer();
    }

}
