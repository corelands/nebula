package core.nebula.lands;

import core.nebula.lands.commands.CmdSetCore;
import core.nebula.lands.commands.CmdSetLocation;
import core.nebula.lands.commands.CmdWorld;
import core.nebula.lands.game.Game;
import core.nebula.lands.game.GameState;
import core.nebula.lands.listeners.*;
import core.nebula.lands.listeners.items.ListenerChooseTeam;
import core.nebula.lands.listeners.items.ListenerLeave;
import core.nebula.lands.listeners.items.ListenerMapVote;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class Core extends JavaPlugin {

    private static Core instance;
    private static Game game;

    public void onEnable() {
        instance = this;
        game = new Game();

        game.setGameState(GameState.WAITING);
        mapLoader();

        registerListeners();
        registerCommands();

        game.getMySQL().createRoom();

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        Bukkit.getServer().setWhitelist(false);

        Core.getGame().getMySQL().setRoomState(GameState.WAITING.toString());
        Core.getGame().getMySQL().setRoomPlayers(0);
    }

    public void onDisable() {
        removeMaps();
        Bukkit.getServer().setWhitelist(false);
    }

    private void registerListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new ListenerPlayerJoin(), this);
        pluginManager.registerEvents(new ListenerPlayerQuit(), this);
        pluginManager.registerEvents(new ListenerPlayerDamage(), this);
        pluginManager.registerEvents(new ListenerMainProtection(), this);
        pluginManager.registerEvents(new ListenerPlayerChat(), this);
        pluginManager.registerEvents(new ListenerChooseTeam(), this);
        pluginManager.registerEvents(new ListenerLeave(), this);
        pluginManager.registerEvents(new ListenerMapVote(), this);
    }

    private void registerCommands() {
        getCommand("setcore").setExecutor(new CmdSetCore());
        getCommand("setlocation").setExecutor(new CmdSetLocation());
        getCommand("world").setExecutor(new CmdWorld());
    }

    private void mapLoader() {
        World lobby = new WorldCreator(getConfig().getString("location.lobby.world")).environment(World.Environment.NORMAL).createWorld();
        lobby.setAutoSave(false);
        lobby.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        lobby.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        lobby.setTime(6000);
        File file = new File("../../../maps/nebula");
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        for(String map : directories) {
            getGame().getGameConfig().addMap(map);
            loadMap(map);
        }
    }

    private void removeMaps() {
        File file = new File("../../../maps/nebula");
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        for(String map : directories) {
            unloadMap(map);
        }
    }

    private void loadMap(String map) {
        Bukkit.getConsoleSender().sendMessage(getGame().getGameConfig().getPrefix() + " §7Loading map §e" + map);
        try {
            FileUtils.copyDirectory(new File("../../../maps/nebula/" + map), new File(map));
        } catch(IOException exc) {

        }
        World world = new WorldCreator(map).environment(World.Environment.NORMAL).createWorld();
        world.setAutoSave(false);
        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        world.setTime(6000);
        Bukkit.getConsoleSender().sendMessage(getGame().getGameConfig().getPrefix() + " §7Map loaded.");
    }

    private void unloadMap(String map) {
        Bukkit.getConsoleSender().sendMessage(getGame().getGameConfig().getPrefix() + " §7Unloading map §e" + map);
        try {
            FileUtils.deleteDirectory(new File(map));
            Bukkit.unloadWorld(Bukkit.getWorld(map), false);
        } catch(Exception exc) {

        }
    }

    public static Game getGame() {
        return game;
    }

    public static Core getInstance() {
        return instance;
    }

}
