package core.nebula.lands.data;

import core.nebula.lands.Core;
import core.nebula.lands.api.ItemCreator;
import core.nebula.lands.api.ParticleEffect;
import core.nebula.lands.game.GameState;
import core.nebula.lands.managers.Hotbar;
import core.nebula.lands.managers.scoreboard.NebulaScoreboardManager;
import core.nebula.lands.managers.teams.Team;
import core.nebula.lands.runnables.StartTimer;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerData {

    private Player player;
    private Player lastHit;
    private boolean gracePeriod;
    private String mapVote;

    public PlayerData(Player player) {
        this.player = player;

        player.setGameMode(GameMode.ADVENTURE);
        teleportLobby();
        giveHotbar();

        this.player.setHealth(20D);
        this.player.setFoodLevel(20);
        this.mapVote = null;

        new NebulaScoreboardManager(player).setScoreboard();

        for(PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }

        this.tryStartGame();
        this.gracePeriod = false;
    }

    public String getName() {
        return this.player.getName();
    }

    public void addDeath(Player killer) {
        this.lastHit = null;

        for(int i = 0; i < 10; i++) {
            ParticleEffect.FIREWORKS_SPARK.display(player, player.getLocation().clone().add(Math.random(), Math.random(), Math.random()), 10, 2);
        }
        player.playSound(player.getLocation(), Sound.ENTITY_GENERIC_HURT, 1f, 1f);
        if(killer == null) {
            Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " " + getTeam().getChatColor() + getName() + " §7died.");
        } else {
            killer.playSound(killer.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1f, 1f);
            Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " " + getTeam().getChatColor() + getName() + " §7has been killed by " + Core.getGame().getPlayerData(killer).getTeam().getChatColor() + killer.getName() + "§7.");
        }

        dropItems();

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getInventory().setItemInMainHand(null);
        player.getInventory().setItemInOffHand(null);
        player.setItemOnCursor(null);
        player.setGameMode(GameMode.SPECTATOR);

        respawn();
    }

    public void addKill() {
        this.player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1f, 1f);
    }

    public Team getTeam() {
        for(Team team : Team.values()) {
            if(team.containsPlayer(player))
                return team;
        }
        return null;
    }

    private void setItems() {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);

        player.getInventory().setHelmet(new ItemCreator(Material.LEATHER_HELMET).setName(getTeam().getChatColor() + "§cHelmet").setColor(getTeam().getColor()).getItem());
        player.getInventory().setChestplate(new ItemCreator(Material.LEATHER_CHESTPLATE).setName(getTeam().getChatColor() + "Chestplate").setColor(getTeam().getColor()).getItem());
        player.getInventory().setLeggings(new ItemCreator(Material.LEATHER_LEGGINGS).setName(getTeam().getChatColor() + "Leggings").setColor(getTeam().getColor()).getItem());
        player.getInventory().setBoots(new ItemCreator(Material.LEATHER_BOOTS).setName(getTeam().getChatColor() + "Boots").setColor(getTeam().getColor()).getItem());

        player.getInventory().setItem(0, new ItemCreator(Material.STONE_SWORD).setName("§7Sword").getItem());
        player.getInventory().setItem(1, new ItemCreator(Material.BOW).setName("§eBow").getItem());
        player.getInventory().setItem(2, new ItemCreator(Material.GOLDEN_APPLE).setAmount(3).setName("§6Golden Apple").getItem());
        player.getInventory().setItem(3, new ItemCreator(Material.OAK_LOG).setAmount(32).setName("§eWood").getItem());
        player.getInventory().setItem(4, new ItemCreator(Material.COOKED_BEEF).setAmount(16).setName("§eBeef").getItem());
        player.getInventory().setItem(5, new ItemCreator(Material.IRON_PICKAXE).setName("§7Pickaxe").getItem());
        player.getInventory().setItem(6, new ItemCreator(Material.IRON_AXE).setName("§7Axe").getItem());
        player.getInventory().setItem(7, new ItemCreator(Material.ARROW).setAmount(12).setName("§7Arrow").getItem());
        player.getInventory().setItem(8, new ItemCreator(Material.NETHER_STAR).setAmount(1).setName("§eSouls").getItem());
    }

    public void spawn() {

        gracePeriod = true;
        new BukkitRunnable() {
            @Override
            public void run() {
                gracePeriod = false;
                player.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Spawn protection removed.");
            }
        }.runTaskLater(Core.getInstance(), 20*5);

        teleportToTeam();
        this.player.setGameMode(GameMode.SURVIVAL);
        this.player.setHealth(20D);
        this.player.setFoodLevel(20);
        setItems();
    }

    public void setLastHit(Player lastHit) {
        this.lastHit = lastHit;
    }

    public Player getLastHit() {
        return this.lastHit;
    }

    public boolean isGrace() {
        return this.gracePeriod;
    }

    public void setMapVote(String mapVote) {
        this.mapVote = mapVote;
    }

    public String getMapVote() {
        return this.mapVote;
    }

    private void respawn() {
        teleportToTeam();

        new BukkitRunnable() {

            int count = 6;

            @Override
            public void run() {
                count--;

                if(count == 0) {
                    teleportToTeam();
                    setItems();
                    player.setGameMode(GameMode.SURVIVAL);
                    player.setHealth(20D);
                    player.setFoodLevel(20);
                    player.setExp(0);
                    player.setLevel(0);

                    player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100D);

                    for(PotionEffect potionEffect : player.getActivePotionEffects()) {
                        player.removePotionEffect(potionEffect.getType());
                    }

                    gracePeriod = true;
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            gracePeriod = false;
                            player.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Spawn protection removed.");
                        }
                    }.runTaskLater(Core.getInstance(), 20*5);

                    this.cancel();
                    return;
                }

                player.sendTitle("§e§lRESPAWNING", "§e" + count + "§7 seconds.");

            }
        }.runTaskTimer(Core.getInstance(), 0, 20);
    }

    public void teleportLobby() {
        this.player.teleport(getLobby());
    }

    private void teleportToTeam() {
        this.player.teleport(getTeam().getSpawnLocation());
    }

    private Location getLobby() {
        FileConfiguration config = Core.getInstance().getConfig();
        return new Location(Bukkit.getWorld(config.getString("location.lobby.world")), config.getDouble("location.lobby.x"), config.getDouble("location.lobby.y"), config.getDouble("location.lobby.z"),config.getLong("location.lobby.yaw"), config.getLong("location.lobby.pitch"));
    }

    private void giveHotbar() {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        for(Hotbar hotbar : Hotbar.values()) {
            player.getInventory().setItem(hotbar.getSlot(), hotbar.getItem());
        }
    }

    private void tryStartGame() {
        if(Bukkit.getOnlinePlayers().size() >= Core.getGame().getGameConfig().getMinPlayers() && Core.getGame().getGameState().equals(GameState.WAITING)) {
            Core.getGame().setGameState(GameState.STARTING);
            new StartTimer().runTaskTimer(Core.getInstance(), 0, 20);
        }
    }

    private void dropItems() {
        try {
            player.getWorld().dropItem(player.getLocation(), player.getItemOnCursor());
        } catch(Exception exc) {

        }
        for(ItemStack itemStack : player.getInventory()) {
            if(itemStack != null) {
                player.getWorld().dropItem(player.getLocation(), itemStack);
            }
        }
    }

}
